package o.y.aws.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import software.amazon.awssdk.regions.internal.util.EC2MetadataUtils;


@RestController
public class AwsController {

    Logger log = LoggerFactory.getLogger(AwsController.class);

    @GetMapping("/region")
    public String awsInstanceRegion() {
        log.info("++++ awsInstanceRegion started" );
        String region = EC2MetadataUtils.getEC2InstanceRegion();
        String availabilityZone = EC2MetadataUtils.getAvailabilityZone();
        log.info("++++ region = " + region);
        log.info("++++ availabilityZone = " + availabilityZone);
        log.info("++++ awsInstanceRegion ended" );
        return region + " " + availabilityZone;
    }
}


